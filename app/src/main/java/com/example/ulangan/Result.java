package com.example.ulangan;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import java.text.CollationElementIterator;

public class Result extends AppCompatActivity {
    public TextView beforeRestult;
    public TextView resultView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        beforeRestult = findViewById(R.id.beforeResultTxtView);
        resultView = findViewById(R.id.resultTxtView);
        DoSetup();
    }

    private void DoSetup() {
        Bundle extras = getIntent().getExtras();
        String type = extras.getString("calcType");
        String panjang = extras.getString("panjang");
        String lebar = extras.getString("lebar");
        String tinggi = extras.getString("tinggi");
        String hasil = extras.getString("hasil");
        String constructedBefore = String.format("%s persegi panjang dengan panjang %s dan lebar %s adalah", , panjang, lebar, tinggi,type);
        beforeRestult.setText(constructedBefore);
        resultView.setText(hasil);
    }
}
