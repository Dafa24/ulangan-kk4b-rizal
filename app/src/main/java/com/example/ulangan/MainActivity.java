package com.example.ulangan;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private TextView textViewPanjang;
    private TextView textViewLebar;
    private TextView textViewTinggi;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textViewPanjang = findViewById(R.id.editTextPanjang);
        textViewLebar = findViewById(R.id.editTextLebar);
        textViewTinggi = findViewById(R.id.editTextTinggi);
    }

    private  int HitungLuas(int panjang, int lebar, int tinggi ) {return  panjang * lebar * tinggi;}
    private  int HitungKeliling(int panjang, int lebar, int tinggi) {return 4 * panjang + lebar + tinggi;}
    private  int HitungVolume(int panjang, int lebar, int tinggi) {return panjang * lebar* tinggi;}

    public void KelilingPressed(View view) {
        if (textViewPanjang != null && textViewLebar != null){
            String panjang = textViewPanjang.getText().toString();
            String lebar = textViewLebar.getText().toString();
            int keliling = HitungKeliling(Integer.parseInt(panjang), Integer.parseInt(lebar));
            Intent intent = new Intent(this,ResultActivity.class);
            intent.putExtra("calcType", "Keliling");
            intent.putExtra("hasil", String.valueOf(keliling));
            intent.putExtra("panjang", panjang);
            intent.putExtra("lebar", lebar);
            startActivity(intent);
        }
    }

    public void LuasPressed(View view) {
        if (textViewPanjang != null && textViewLebar != null){
            String panjang = textViewPanjang.getText().toString();
            String lebar = textViewLebar.getText().toString();
            int keliling = HitungKeliling(Integer.parseInt(panjang), Integer.parseInt(lebar));
            Intent intent = new Intent(this,ResultActivity.class);
            intent.putExtra("calcType", "Luas");
            intent.putExtra("hasil", String.valueOf(keliling));
            intent.putExtra("panjang", panjang);
            intent.putExtra("lebar", lebar);
            startActivity(intent);
    }
    public void
}

    public void VolumePressed(View view) {
        if (textViewPanjang != null && textViewLebar != null){
            String panjang = textViewPanjang.getText().toString();
            String lebar = textViewLebar.getText().toString();
            int keliling = HitungKeliling(Integer.parseInt(panjang), Integer.parseInt(lebar));
            Intent intent = new Intent(this,ResultActivity.class);
            intent.putExtra("calcType", "Volume");
            intent.putExtra("hasil", String.valueOf(keliling));
            intent.putExtra("panjang", panjang);
            intent.putExtra("lebar", lebar);
            startActivity(intent);
    }
